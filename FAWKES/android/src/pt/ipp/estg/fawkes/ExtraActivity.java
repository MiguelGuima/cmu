package pt.ipp.estg.fawkes;
;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

public class ExtraActivity extends AppCompatActivity {

    private Toolbar toolbar_extra = null;
    String[] inteiros = {"0", "1", "2", "3", "4", "5"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.extraactivity_layout);

        GridView gridview = (GridView) findViewById(R.id.extra_gridview);
        ArrayAdapter<String> my_adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, inteiros);
        gridview.setAdapter(my_adapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Toast.makeText(ExtraActivity.this, "" + position,
                        Toast.LENGTH_SHORT).show();
                if(position==0){
                    Intent i = new Intent(getApplicationContext(), NivelUm.class);
                    startActivity(i);
                }
            }
        });


        toolbar_extra = (Toolbar) findViewById(R.id.toolbar_extra);
        toolbar_extra.setTitle("Vidas"); //Definir titulo à toolbar
        setSupportActionBar( toolbar_extra ); //Método essencial de suporte do toolbar(sem este método não aparece as opções de menu)
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menu_inflater = getMenuInflater();
        menu_inflater.inflate(R.menu.menu_extra, menu);
        return true;
    }

}
