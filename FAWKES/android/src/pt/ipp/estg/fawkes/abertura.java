package pt.ipp.estg.fawkes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class abertura extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.abertura_layout);

        Thread thread_abertura = new Thread(){
            @Override
            public void run(){
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent i = new Intent(abertura.this, MainActivity.class);
                    startActivity(i);
                }
            }
        };
        thread_abertura.start();
    }

    @Override
    protected void onPause(){
        super.onPause();
        finish();
    }
}
