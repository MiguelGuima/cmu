package pt.ipp.estg.fawkes;

import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //Botões do menu principal
    private Button jogo = null;
    private Button extra = null;
    private Button config = null;
    private Button info = null;
    private Toolbar este_toolbar = null; //Toolbar do menu principal

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainactivity_layout);


        jogo = (Button) findViewById(R.id.button_jogar);
        extra = (Button) findViewById(R.id.button_extra);
        info = (Button) findViewById(R.id.item_info_main);

        este_toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        este_toolbar.setTitle("Vidas"); //Definir titulo à toolbar
        setSupportActionBar( este_toolbar ); //Método essencial de suporte do toolbar(sem este método não aparece as opções de menu)
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menu_inflater = getMenuInflater();
        menu_inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    public void button_jogar (View v){
        Intent i = new Intent(this, AndroidLauncher.class);
        startActivity(i);
    }

    public void button_extra (View v){
        Intent i = new Intent(getApplicationContext(), ExtraActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){
            case R.id.item_info_main:
                Toast.makeText(this, "Para jogar clique no botão JOGAR, para ganhar vidas clique no botão EXTRA", Toast.LENGTH_LONG).show();
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

}
