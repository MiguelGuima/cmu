package pt.ipp.estg.fawkes;


import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


public class NivelUm extends AppCompatActivity {

    private Button button_play;
    private SeekBar positionBar, volumeBar;
    private TextView tempo_inicial, tempo_restante;
    private MediaPlayer mediaPlayer;
    private  int totalTime;
    private Button confirmar = null;
    private EditText confirmar_texto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.nivelum_layout);

        confirmar = (Button) findViewById(R.id.nivel1_button_confirmar);
        button_play = (Button) findViewById(R.id.nivel1_playButton);
        tempo_inicial = (TextView) findViewById(R.id.nivel1_tempo_inicial);
        tempo_restante = (TextView) findViewById(R.id.nivel1_tempo_falta);
        confirmar_texto = (EditText) findViewById(R.id.nivel1_editText);


        mediaPlayer = MediaPlayer.create(NivelUm.this, R.raw.despacito);
        mediaPlayer.setLooping(true);
        mediaPlayer.seekTo(0);
        mediaPlayer.setVolume(0.5f, 0.5f);
        totalTime = mediaPlayer.getDuration();

        positionBar = (SeekBar) findViewById(R.id.nivel1_seekbar_positionBar);
        positionBar.setMax(totalTime);

        positionBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser) {
                    mediaPlayer.seekTo(progress);
                    positionBar.setProgress(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
            });

        volumeBar = (SeekBar) findViewById(R.id.nivel1_seekbar_volume);
        volumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float volumeNumber = progress/100f;
                mediaPlayer.setVolume(volumeNumber, volumeNumber);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (mediaPlayer != null) {
                    try{
                        Message message = new Message();
                        message.what = mediaPlayer.getCurrentPosition();
                        handler.sendMessage(message);
                        Thread.sleep(1000);
                    }catch (InterruptedException e) {}
                }
            }
        });
    }

    private Handler handler = new Handler() {

        @Override
        public  void handleMessage(Message message) {

            int currentPosition = message.what;
            positionBar.setProgress(currentPosition);

            String texto_inicial = createTimeLable(currentPosition);
            tempo_inicial.setText(texto_inicial);

            String texto_restante = createTimeLable(totalTime-currentPosition);
            tempo_restante.setText("- "+ texto_restante);
        }
    };

    public String createTimeLable (int time){

        String timeLable = "";
        int min = time/1000/60;
        int sec = time/1000%60;

        timeLable = min + ":";
        if(sec<10)
            timeLable += "0";
        timeLable += sec;

        return timeLable;
    }

    public void button_play(View v){
        if(!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
            button_play.setBackgroundResource(R.drawable.stop);
        }else {
            mediaPlayer.pause();
            button_play.setBackgroundResource(R.drawable.play);

        }
    }

    public void nivel1_button_confirmar (View v){
        if (confirmar_texto.getText().toString().equals("despacito")) {
            Toast.makeText(this, "Parabéns, mais uma vida.", Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, ExtraActivity.class);
            startActivity(i);

        }else{
            Toast.makeText(this, "Resposta errada, tenta novamente.", Toast.LENGTH_LONG).show();
        }
    }

}
